The first challenge is the interview challenge that would be set for candidates applying for a job in the iOS team at my last company. Candidates would normally have up to an hour to complete the test, encouraged to use the Internet and ask for help. The main goal of the test was to identify how a candidate would approach a problem and how they would structure their code, debug any issues and to demonstrate they were aware of the basics of iOS development.
GitHub/BitBucket links of projects are encouraged so everyone can share their solutions and discuss different approaches. Objective-C or Swift.

# **iOS Challenge** - [Image of test](http://imgur.com/FSR4qdA)
Thanks for taking the time to take the iOS interview challenge. Don’t worry the challenge is not super hard - it is designed so we can assess how you would go about programming in your day-to-day job. We are looking for more than just working code: comments, good structure and an understanding of how to test your code.
You are free to use Google and the internet and encouraged to make use of your interviewers should you get stuck or have any questions.
Create an application that initially displays a form with controls to enter a username and password and submit the form. When the form is submitted ensure:
* The username is at least 8 characters in length and alphabetic
* The password is at least 8 characters in length

Then navigate to another page and display an image and a button to return to the previous screen. When tapped the image should show a message saying “Hello World”. If the user returns to the previous screen all information in the form should be cleared.

For bonus points demonstrate an understanding of how you would test your code.