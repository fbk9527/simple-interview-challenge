#import <Foundation/Foundation.h>

@interface NSString (SimpleInterviewChallengeStringValidation)
-(BOOL)doesInputMeetMinimumLength:(NSUInteger)minimumLength;
-(BOOL)isUsernameAlphabeticOnly;
@end
