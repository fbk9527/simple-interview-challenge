#import <UIKit/UIKit.h>

#import "SICAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SICAppDelegate class]));
    }
}
