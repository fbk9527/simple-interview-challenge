#import <UIKit/UIKit.h>
#import "SICImagePreviewViewController.h"
@interface SICUserInputViewController : UIViewController <SICImagePreviewDelegate, UITextFieldDelegate>
@end
