#import "NSString+SimpleInterviewChallengeStringValidation.h"
#include <ctype.h>

@implementation NSString (SimpleInterviewChallengeStringValidation)

-(BOOL)doesInputMeetMinimumLength:(NSUInteger)minimumLength{
    return ([self length] >= minimumLength);
}

-(BOOL)isUsernameAlphabeticOnly{
    NSUInteger len = [self length];
    for (int i = 0; i < len; i++) {
        unichar charAtIndex = [self characterAtIndex:i];
        if (isalpha(charAtIndex) == 0) {
            return NO;
        }
    }
    return YES;
}
@end
