#import "SICImagePreviewViewController.h"

@interface SICImagePreviewViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imagePreview;
@property (weak, nonatomic) IBOutlet UIButton *returnButton;
@property (weak, nonatomic) IBOutlet UIButton *RequestReturn;
@property (weak, nonatomic) IBOutlet UILabel *helloWorldLabel;
@end

@implementation SICImagePreviewViewController

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    return self;
}

-(void)viewDidLoad{
    [self.imagePreview setUserInteractionEnabled:YES];
    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap:)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    [self.imagePreview addGestureRecognizer:tap];
}

- (void)handleTap:(UITapGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateEnded)
    {
        self.helloWorldLabel.alpha =  1.0;
    }
}

-(void)viewWillAppear:(BOOL)animated{
    self.helloWorldLabel.alpha = 0.0f;
}
- (IBAction)returnToUserInputScreen:(id)sender {
    [self.delegate dismissImagePreviewController:self];
}
@end
