#import <UIKit/UIKit.h>

@class SICImagePreviewViewController;
@protocol SICImagePreviewDelegate
- (void)dismissImagePreviewController:(SICImagePreviewViewController*)preview;
@end

@interface SICImagePreviewViewController : UIViewController
@property(weak,nonatomic) id<SICImagePreviewDelegate> delegate;
@end


