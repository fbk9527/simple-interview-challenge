#import "SICUserInputViewController.h"
#import "NSString+SimpleInterviewChallengeStringValidation.h"

#define SEGUE @"loginsuccess"
#define PASSWORD_MINIMUM_LENGTH 8
#define USERNAME_MINIMUM_LENGTH 8

@interface SICUserInputViewController ()
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;
-(void)reset;
-(BOOL)validate;
@end

@implementation SICUserInputViewController

-(void)viewDidLoad{
    self.errorLabel.alpha = 0.0f;
    self.usernameTextField.delegate = self;
    self.passwordTextField.delegate = self;
}
- (IBAction)userRequestLogin:(id)sender {
    if ([self validate]) {
        [self performSegueWithIdentifier:SEGUE sender:self];
    }
}

#pragma mark - Helpers
-(void)reset{
    self.usernameTextField.text = @"";
    self.passwordTextField.text = @"";
}

-(BOOL)validate{
    
    BOOL valid = YES;
    valid = valid && [self.usernameTextField.text doesInputMeetMinimumLength:USERNAME_MINIMUM_LENGTH];
    valid = valid && [self.usernameTextField.text isUsernameAlphabeticOnly];
    if (!valid) {
        self.errorLabel.text = @"Invalid Username!";
        __weak UILabel* txtField = self.errorLabel;
        [UIView animateWithDuration:1.0f/10.0f animations:^{
            txtField.alpha = 1.0f;
        } completion:^(BOOL finished){
            [UIView animateWithDuration:2.0f animations:^{
                txtField.alpha = 0.0f;
            }];
        }];
    } else {
        valid = valid && [self.passwordTextField.text doesInputMeetMinimumLength:PASSWORD_MINIMUM_LENGTH];
        if (!valid) {
            self.errorLabel.text = @"Invalid Password!";
            __weak UILabel* txtField = self.errorLabel;
            [UIView animateWithDuration:1.0f/10.0f animations:^{
                txtField.alpha = 1.0f;
            } completion:^(BOOL finished){
                [UIView animateWithDuration:1.5f animations:^{
                    txtField.alpha = 0.0f;
                }];
            }];
    }
    
    
    }
    return valid;
}
#pragma mark - Delegate
-(void)dismissImagePreviewController:(SICImagePreviewViewController *)preview{
    [self reset];
    [preview dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Segue Handling
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{

    if ([[segue identifier]isEqualToString:SEGUE]) {
        // Set delegate
        SICImagePreviewViewController* previewController = (SICImagePreviewViewController*)[segue destinationViewController];
        [previewController setDelegate:self];
    }
}

#pragma mark - Textfield Delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self userRequestLogin:nil];
    return YES;
}
@end
